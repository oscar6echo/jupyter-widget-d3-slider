var d3_slider = require('./index');
var base = require('@jupyter-widgets/base');

module.exports = {
  id: 'jupyter-widget-d3-slider',
  requires: [base.IJupyterWidgetRegistry],
  activate: function(app, widgets) {
      widgets.registerWidget({
          name: 'd3_slider',
          version: d3_slider.version,
          exports: d3_slider
      });
  },
  autoStart: true
};
