
# `jupyter-widget-d3-slider` Javascript part


This is the javascript part of a demo [jupyter-widget](https://ipywidgets.readthedocs.io/en/stable/).

Check out the [jupyter-widget-d3-slider repo](https://gitlab.com/oscar6echo/jupyter-widget-d3-slider/tree/master) for more info.  


## 1 - Prerequisite

Install [node](http://nodejs.org/).  

## 2 - Install

```bash
npm install --save jupyter-widget-d3-slider
```
