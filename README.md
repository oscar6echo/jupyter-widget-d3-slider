# jupyter-widget-d3-slider

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gh/ocoudray/jupyter-d3-slider-binder/master?filepath=Slider_d3_demo.ipynb)

## 1 - Overview

This is a [jupyter widget (or ipywidget)](https://ipywidgets.readthedocs.io/en/stable/) wrapping a [simple custom slider](https://bl.ocks.org/mbostock/6452972) based on the fantastic [d3.js](https://d3js.org/) library.  

The official documentation describes a [Hello World](https://ipywidgets.readthedocs.io/en/stable/examples/Widget%20Custom.html) widget and also points to several very [advanced widgets](http://jupyter.org/widgets.html) from [bqplot](https://github.com/bloomberg/bqplot) to [ipyvolume](https://github.com/maartenbreddels/ipyvolume).  
But there is a large gap between these 2 stages.  

This simple widget may serve as a template for intermediate users who feel the (huge) potential of this library, want to build one, but cannot find examples relevant to their level i.e. past absolute beginner.

To get a feel of the result check out the [demo notebook](http://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/jupyter-widget-d3-slider/raw/master/notebooks/demo_d3_slider.ipynb).  

If you are interesting about how to embed jupyter widgets in a pure web context, check out the [second notebook](http://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/jupyter-widget-d3-slider/raw/master/notebooks/demo_export_widgets.ipynb)

For more info about jupyter widgets (installation process, packaging and publishing), see [this tutorial repo](https://github.com/ocoudray/first-widget). All what's written there is also true for this package, just changing the name `first-widget` into `jupyter-widget-d3-slider`.

## 2 - Installation

    $ pip install jupyter_widget_d3_slider

